# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=tree-sitter-java
pkgver=0.19.1
pkgrel=0
pkgdesc="Java grammar for tree-sitter"
url="https://github.com/tree-sitter/tree-sitter-java"
arch="all"
license="MIT"
makedepends="tree-sitter-dev"
install_if="tree-sitter-grammars"
source="https://github.com/tree-sitter/tree-sitter-java/archive/v$pkgver/tree-sitter-java-$pkgver.tar.gz"
options="!check"  # no tests for shared lib

build() {
	cd src

	cc $CFLAGS -fPIC -Wall -std=c99 -c ./*.c
	cc $LDFLAGS -shared -Wl,--no-undefined -o lib$pkgname.so ./*.o
}

package() {
	install -D -m644 src/lib$pkgname.so -t "$pkgdir"/usr/lib/

	# Create symlink for Neovim
	install -d "$pkgdir"/usr/share/nvim/runtime/parser
	ln -s ../../../../lib/lib$pkgname.so \
		"$pkgdir"/usr/share/nvim/runtime/parser/${pkgname#tree-sitter-}.so
}

sha512sums="
71bd34132785f1b95538ce321eba4d6258fff309abf6150852b749336d7ffa5e4bd06f52fb5b6db2e2845592e6faf2ad4cac226289a0078f06cf2fc2f74152bb  tree-sitter-java-0.19.1.tar.gz
"
